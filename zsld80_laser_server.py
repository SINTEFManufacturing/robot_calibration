#!/usr/bin/env python

"""
Module for simple reading from the Omron ZSLD80 laser distance sensor via the UE9 DAQ board.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Lars Tingelstad", "Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import time
import threading
import socket
import struct

import ue9

class ZSLD80LaserConnector(object):

    def __init__(self, channel=0):
        self._dev = ue9.UE9()
        self._channel = channel
        self._v2d_gain = 0.03 / 5.0
        self._dist_offset = 0.065
        ## Offset correction to transducer box output. Experimentally
        ## identified.
        self._v_offset = 0.042

    def _get_distance_measurement(self):
        voltage = self.voltage
        if voltage > 5.09:
            print('ZSLD80: Out of measuring range')
        else:
            distance = self._dist_offset + (self._v2d_gain * voltage)
            return distance

    @property
    def voltage(self):
        return self._dev.readRegister(self._channel * 2) + self._v_offset

    @property
    def distance(self):
        return self._get_distance_measurement()

if __name__ == '__main__':
    import sys
    laser = ZSLD80LaserReader()
    if 'server' in sys.argv:
        out_addr = ('127.0.0.1', 4999)        
        out_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        out_struct = struct.Struct('d')
        while True:
            out_sock.sendto(out_struct.pack(laser.voltage), out_addr)
