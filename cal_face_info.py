"""
Calibration face (plane) info structure module.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import pickle

import math3d as m3d

class CalFaceInfo(object):
    """Retrieve, hold, and store data for a calibration face."""
    
    def __init__(self, pose, coordinates, name):
        """Arg must be a transform 'pose' and a set of xy-plane
        'coordinates' specifying a calibration face in 'pose'
        coordinates, by its rectangular polygon. The calibration face
        info object will be named according to 'name'."""
        self._name = name
        self._coords = coordinates
        self._pose = pose
        self.delta_x = max([c.x for c in self._coords])
        self.delta_y = max([c.y for c in self._coords])
        self.diagonal = m3d.Vector(self.delta_x, self.delta_y, 0.0)
        # print(self._diagonal)

    @property
    def name(self):
        return self._name

    @property
    def pose(self):
        return self._pose

    @property
    def normal(self):
        return self._pose.orient.vec_z

    @property
    def origo(self):
        return self._pose.pos

    @property
    def x_hat(self):
        return self._pose.orient.vec_x
    
    @property
    def y_hat(self):
        return self._pose.orient.vec_y
    
    @property
    def dimensions(self):
        return self.diagonal
    
    @property 
    def vertices(self):
        return self._coords

    def store(self, folder='', name=None):
        if name is None:
            name = self._name
        file_name = os.path.join(folder, name+'.cal_face.pickle')
        store_file = open(file_name, 'wb')
        pickle.dump(self, store_file)
        store_file.close()
        
