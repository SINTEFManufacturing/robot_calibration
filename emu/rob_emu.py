"""
Module for setting up a simple robot emulation in a calibration.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys
import time

import math3d as m3d
import numpy as np

import bge
    
from emulib import orbiter, mouse, utils
import emulib.utils

from pymoco.simulator.ur_router_tcp_emu import URRouterTCPEmu
import pymoco.robot

from emulator_options import emu_args

_initialized = False

_setup_folder = '../setup/'
_offset_vector_file = os.path.join(
    _setup_folder, 
    'emu_joint_offset_vector.npy.txt')
_cyl_roll_vector_file = os.path.join(
    _setup_folder, 
    'emu_cylinder_roll_vector.npy.txt')
#_cached_offset_vector_file = 'cached_emu_joint_offset_vector.npy.txt'

def init():
    global cal_probe, rob_emu, probe_center_go, _initialized
    print('Rob Emu initializing')
    #scene = bge.logic.getCurrentScene()
    scene = bge.logic.getCurrentScene()
    # // Setup the robot
    router_init_dict = {}
    rob_base_go =  emulib.utils.get_gos_by_prop(prop='Type', propVal='RobotBase')[0]
    np.savetxt(os.path.join(_setup_folder, 'rob_base_pose.npy.txt'), 
            emulib.utils.mu2m3d(rob_base_go.worldTransform).array)
    router_init_dict['rob_base_go'] = rob_base_go
    rob_type = rob_base_go['RobotType']
    rob_def = pymoco.robot.create_robot(rob_type)
    router_init_dict['rob_def'] = rob_def
    if os.path.isfile(_offset_vector_file):
        print('Loading offset vector from file "{}"'.format(_offset_vector_file))
        rob_def.encoder_offsets = np.loadtxt(_offset_vector_file)
    else:
        print('Saving default offset vector to file "{}"'.format(_offset_vector_file))
        np.savetxt(_offset_vector_file, rob_def.encoder_offsets)
    if os.path.isfile(_cyl_roll_vector_file):
        print('Loading kinematic parameter vector from "{}"'.format(
                _cyl_roll_vector_file))
        rob_def.cylinder_rolls = np.loadtxt(_cyl_roll_vector_file)
    else:
        print('Saving kinematic parameter vector to "{}"'.format(
                _cyl_roll_vector_file))
        np.savetxt(_cyl_roll_vector_file, rob_def.cylinder_rolls)

    print('Roll line inner = {}'.format(rob_def.cylinder_roll_lines[0]))
    # else:
    #     print('Setting default offset vector')
    #     rob_def.encoder_offsets = np.array(
    #         [0.01,-0.02, 0.01, 0.02, 0.03, -0.01], dtype=np.float64)
    print('Joint offset vector in use: {}'.format(rob_def.encoder_offsets))
    np.savetxt(_offset_vector_file, rob_def.encoder_offsets)
    print('robot base GO: {}'.format(rob_base_go))
    # connection_type = rob_base_go.get('connection_type', 
    if 'port' in rob_base_go:
        port = int(rob_base_go['port'])
        router_init_dict['gw_port'] = port
    router_init_dict['bind_host'] = ''
    # rob_emu = URRouterTCPEmu(**router_init_dict)
    rob_emu = URRouterTCPEmu(rob_base_go, rob_def,
                             bind_host=emu_args.rob_host,
                             gw_port=emu_args.rob_port,
                             log_level=2)
    rob_emu.start()
    print('Rob Emu initialized')
    _initialized = True

p_est = 0.0
t_last = time.time()

def time_update():
    global t_last, p_est
    if not _initialized:
        print('Rob Emu not initialized!')
        return
    rob_emu.emit_state()
    #t = time.time()
    #dt = t - t_last
    #p_est += dt
    #p_est *= 0.5
    #print('cal_emu.time_update : %.3fs (%.3fHz)' % (p_est, 1.0/p_est))
    #t_last = t
    time.sleep(0.001)
    #print(probe_center_go.worldPosition)



_quitting = False
def quit():
    global _quitting, rob_emu
    if not _quitting:
        _quitting = True
        print('Quitting')
        rob_emu.stop()
