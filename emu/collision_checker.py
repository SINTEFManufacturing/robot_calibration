"""
Collision checking facilities. Initialization of collision body trackers and collision event server.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import socket
import time
import struct

import bge

from emulator_options import emu_args

coll_sensors = []
coll_trackers = []
_initialized = False

debug = 0

class CollBodyTracker(object):
    def __init__(self, sensor):
        self._sensor = sensor
        self._sensor.reset()
        self._init_coll_set = set(self._sensor.hitObjectList)
        self.coll_set = set([])
        # self._coll_state = False
        self.name = self._sensor.owner.name
        if debug > 1:
            print('CollTracker ({}) init : {} ({})'.format(
                    self.name, self._init_coll_set, 
                    self._sensor.hitObjectList))
    
    @property
    def collision(self):
        # if not self._sensor.triggered:
        #     return self._coll_state
        self._sensor.hitObjectList
        self.coll_set = set(self._sensor.hitObjectList)-self._init_coll_set
        if debug > 2: 
            print('CollTracker ({}) check : {}'.format(
                    self.name, self._sensor.hitObjectList))
        return len(self.coll_set) > 0

class CollisionEventServer(threading.Thread):
    def __init__(self, collision_sensors, addr=('127.0.0.1',10099)):
        threading.Thread.__init__(
            self, name='CollisionEmuService <{}>'.format(addr))
        self.daemon = True
        self._addr = addr
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind(self._addr)
        self._sensors_initialized = False
        self._coll_sensors = collision_sensors
        self.__stop = False
        self._client_addrs = set([])

    def run(self):
        while not self.__stop:
            pkg = self._socket.recvfrom(4096)
            data, addr = pkg
            cmd = data.decode()
            if cmd in ['1', 'connect', 'disconnect']:
                if cmd == 'connect':
                    print('Client {} connected'.format(addr))
                    self._client_addrs.add(addr)
                elif cmd == 'disconnect':
                    print('Client {} disconnected'.format(addr))
                    self._client_addrs -= set([addr])
                else:
                    print('Client {} requested "1"'.format(addr))
                self._notify(addr)

    def _notify(self, client_addr=None):
        if client_addr is None:
            for ca in self._client_addrs:
                self._socket.sendto(
                    struct.pack('fB',self._coll_event_time, self._coll), ca)
        else:
            # Send only to the given client
            self._socket.sendto(
                struct.pack('fB',self._coll_event_time, self._coll), client_addr)

    def check(self):
        self._coll_event_time = time.time()
        self._sensors_initialized
        if not self._sensors_initialized:
            self._coll_trackers = [CollBodyTracker(cs) for cs in self._coll_sensors]
            self._sensors_initialized = True
        coll = False
        for ct in self._coll_trackers:
            if ct.collision:
                coll = True
                if debug > 0:
                    print('COLLISION {} : {}'.format(ct.name, ct.coll_set))
        self._coll = coll
        if coll:
            print('Collision detected')
        self._notify()
        return coll

    def stop(self):
        self.__stop = True


def init():
    global  check, ccs
    cont = bge.logic.getCurrentController()
    coll_sensors = cont.owner.controllers['coll_chk'].sensors
    if emu_args.coll_checker:
        print('Setting up collision detection server')
        bge.logic.setLogicTicRate(emu_args.coll_checker_tic_rate)
        ccs = CollisionEventServer(coll_sensors, addr=(emu_args.coll_event_host,emu_args.coll_event_port))
        ccs.start()
        check = ccs.check
    else:
        check = lambda : None
        for cs in coll_sensors:
            cs.owner.visible = False

