"""
Main and utilities module for the calibration emulation scene.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import site

import sys
sys.path.append('..')

_setup_folder = '../setup/'

import math3d as m3d
import numpy as np
import bge

import emulib.utils

from cal_face_info import CalFaceInfo
import emulator_options

def cal_face_info_factory(cal_face):
    name = cal_face.name
    mesh = cal_face.meshes[0]
    poly = mesh.getPolygon(0)
    matid = poly.material_id
    verts = [mesh.getVertex(matid,poly.getVertexIndex(v)) for v in range(4)]
    coordinates = [m3d.Vector(v.XYZ[:]) for v in verts]
    pose = emulib.utils.mu2m3d(cal_face.worldTransform)
    return CalFaceInfo(pose, coordinates, name)


def init():
    print('cal_laser_emu.init')
    # global laser
    # laser = bge.logic.getCurrentScene().objects['laser_ray']
    # print('laser in world (init):',laser.worldPosition)
    cal_pole = bge.logic.getCurrentScene().objects['cal_pole']
    cal_faces = emulib.utils.get_gos_by_prop(prop='cal_face')
    print('Calibration faces: '+str(cal_faces))
    for cal_face in cal_faces:
        cal_face.removeParent()
        cfi = cal_face_info_factory(cal_face)
        cfi.store(folder=_setup_folder)

    ## Explicit extraction
    # face_x_n = cal_pole.worldOrientation.col[0]
    # face_x_p = cal_pole.worldPosition + 0.5 * cal_pole_width * face_x_n
    # face_x_nposv = (face_x_p * face_x_n) * face_x_n
    # face_x_npv =  face_x_nposv / face_x_nposv.length**2
    # np.save('cal_plate_x',np.vstack((face_x_n,face_x_p,face_x_npv)))
    # face_y_n = cal_pole.worldOrientation.col[1]
    # face_y_p = cal_pole.worldPosition + 0.5 * cal_pole_width * face_y_n
    # face_y_nposv = (face_y_p * face_y_n) * face_y_n
    # face_y_npv =  face_y_nposv / face_y_nposv.length**2
    # np.save('cal_plate_y',np.vstack((face_y_n,face_y_p,face_y_npv)))


# def time_update():
#     print('laser in world (update):',laser.worldPosition)
    

_quitting = False
def quit():
    global _quitting, rob_emu
    if not _quitting:
        _quitting = True
        print('Quitting')
        co = bge.logic.getCurrentController()
        actuator = co.actuators["exit"]
        co.activate(actuator)

    
