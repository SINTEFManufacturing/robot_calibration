"""
Module with facilities for emulation of a tool-mounted laser distance
sensor with the Blender Game Engine.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import threading
import socket
import struct

import numpy as np
import mathutils
import emulib.find_utils as fu

from emulator_options import emu_args

_setup_folder = '../setup/'

#class LaserToolParameters(object):
    
class LaserSensorTool(threading.Thread):

    def __init__(self, tool_base_go, host='', port=4999, log_level=2):
        threading.Thread.__init__(self,name='LaserSensorTool')
        self.daemon = True
        self._log_level = log_level
        self._tb_go = tool_base_go
        self._log('Creating sensor on "{}"'.format(self._tb_go), 5)
        self._log('Searching for laser ray object among children: {}'.format(str(self._tb_go.childrenRecursive)), 5)
        self._ray_go = fu.get_gos_by_prop(self._tb_go.childrenRecursive, prop='laser_ray')[0]
        self._log('Found "laser_ray" object "{}"'.format(str(self._ray_go)), 5)
        ## Get some nominal position and parametrized orientation of
        ## the laser ray in flange coordinates
        self._dump_parameters()
        self._dist = -1.0
        self._dist_time = -1.0
        self._dist_limits = [0.065,0.095]
        self._out_of_range = 5.1
        self._output_voltage=[0.0,5.0]
        self._gain = (self._output_voltage[1] - self._output_voltage[0]) / (self._dist_limits[1] - self._dist_limits[0])
        self._host = host
        self._port = port
        self._udp_out = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def _dump_parameters(self):
        w_flange = self._tb_go.worldTransform
        w_ray = self._ray_go.worldTransform
        f_ray = w_flange.inverted() * w_ray
        self._log('Ray in flange:\n{}'.format(f_ray), 4)
        f_ray_z = mathutils.Vector(f_ray.col[2][:3])
        f_ray_p = mathutils.Vector(f_ray.col[3][:3])
        f_ray_phiy = np.arccos(f_ray_z.z)
        f_ray_phiz = np.arctan2(f_ray_z.y, f_ray_z.x)
        self._log('Ray Z in flange:\n{}'.format(f_ray_z), 4)
        self._log('Ray phi_y={} phi_z={}'.format(*[np.rad2deg(f) for f in [f_ray_phiy,f_ray_phiz]]), 5)
        self._log('Ray pos in flange:\n{}'.format(f_ray_p), 4)
        parameters = np.append(np.array(f_ray_p), [f_ray_phiy, f_ray_phiz])
        self._log('Saving parameters: {}'.format(parameters), 4)
        np.savetxt(os.path.join(_setup_folder, 'laser_sensor_parameters.npy.txt'), parameters)
        

    def _log(self, message,level=2):
        if level <= self._log_level:
            print('LaserSensorTool: {}'.format(str(message)))

    def _bge_update(self):
        """Perform a ray trace to measure distance"""
        sensor_dir = self._ray_go.worldOrientation.transposed()[2]
        sensor_pos = self._ray_go.worldPosition
        self._log('sensor dir = {}, position = {}'.format(sensor_dir, sensor_pos), 5)
        o, h, n = self._ray_go.rayCast(sensor_pos+sensor_dir, None, 0, '')
        self._log('Ray hit: {} {} {}'.format(o,h,n), 5)
        if h is None:
            meas_v = self._out_of_range
        else:
            dist = (h-self._ray_go.worldPosition).length
            self._log('Ray hit dist: {}'.format(dist), 5)
            if dist > self._dist_limits[1] or dist < self._dist_limits[0]:
                meas_v = self._out_of_range
            else:
                meas_v = self._gain * (dist-self._dist_limits[0])
        self._log('Output voltage: {}'.format(meas_v), 5)
        self._udp_out.sendto(struct.pack('d',meas_v), (self._host,self._port))

update = lambda:None

def init():
    if not emu_args.coll_checker:
        # // Only set up laser sensor server if we are real emulator.
        global _laser_sensor_tool, update
        _laser_sensor_tool = LaserSensorTool(
            fu.get_gos_by_prop(prop='laser_tool')[0], log_level=4)
        update = _laser_sensor_tool._bge_update
    
    
