"""
Module for options to the emulation system.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import argparse

sysargs = sys.argv[:]
if '--' in sysargs:
    sysargs.remove('--')
parser = argparse.ArgumentParser()
parser.add_argument('--coll_checker', action='store_true', default=False,
                    help='''Set up for collision detection if switched
                    on. The collsion checker will provide service over
                    UDP on the host given in "--coll_event_host" and
                    the port given in "--coll_event_port".'''
                    )
parser.add_argument('--coll_checker_tic_rate', type=int, default=30,
                    help='''If collision checker is set up,
                    i.e. --coll_checker switch i set, this option
                    gives a tic rate to run the emulator in. Lower tic
                    rate lowers the load from the collision checker
                    emulator, but also increases the probability of
                    undetected collisions.'''
                    )
parser.add_argument('--coll_event_host', default='127.0.0.1',
                    help='''If collsion checking, this option gives
                    the host name which the collision service should
                    bind to.'''
                    )
parser.add_argument('--coll_event_port', type=int, default=10099,
                    help=''''If collsion checking, this option gives
                    the port on which which the collision service
                    should listen for client connections.'''
                    )
parser.add_argument('--rob_host', type=str, default='127.0.0.1',
                    help='''The host on which to bind the robot
                    controller emulator to.'''
                    )
parser.add_argument('--rob_port', type=int, default=5002,
                    help='''The port on which the robot controller
                    emulator should listen for control connections.'''
                    )
parser.add_argument('--offset_id', type=str, default='',
                    help='''The idenfification of the offset vector to
                    use. This will be loaded from the file
                    "emu_joint_offset_vector_{offset_id}.npy.txt" in
                    the setup folder.'''
                    )
parser.add_argument('--cyl_roll_id',
                    type=str, default='',
                    help='''Idenfication of the kinematic parameter
                    vector to use in the emulation. This will be
                    loaded from the file
                    "emu_cylinder_roll_vector_{kin_par_id}.npy.txt" in
                    the setup folder.'''
                    )
#parser.add_argument('rargs', nargs=argparse.REMAINDER)
emu_args,rargs = parser.parse_known_args(sysargs)
print('Emulator arguments : ',emu_args)
