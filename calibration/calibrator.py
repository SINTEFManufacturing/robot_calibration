#!/usr/bin/env python3

"""
Script for the performing of calibration analysis for a robot, an LDS,
and one or more planes used for distance measurement objects. All
controls and parameters for the calibration are set up and configured
here, and a calibration can be done by default.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys
import pickle
import collections

import math3d as m3d
import numpy as np
from pymoco.robot.ur5_ml_cyl_rolls import UR5_ML_CylRolls

sys.path.append('..')
from lds_par import LDSExtPar

from plane import Plane
from lds_rob_cal import LDSRobCal

_setup_folder = '../setup/'
_data_folder = '../data/'

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('face_ids', nargs='*', default=['west', 'north', 'top'],
                    help='''The IDs of the calibration faces which is
                    to be used for calibration.'''
)
parser.add_argument('--sample_sets_id', default='',
                    help='''The sample sets are grouped in the data
                    folder. This option identifies the sub-folder in
                    the data folder for the sample sets to calibrate
                    on.'''
                    )
parser.add_argument('--emu_id', default='',
                    help='''If emulated data are used, this identifies
                    the joint offset vector used during emulated data
                    sampling. I.e. the joint vector to be
                    identified.'''
                    )
parser.add_argument('--n_cal', type=int, default=30,
                    help='''Total number of samples to include in
                    parameter identification.'''
                    )
parser.add_argument('--run_type', default='emu', choices=['emu', 'real'],
                    help='''Select whether the data are from a real or
                    an emulated run.'''
                    )
parser.add_argument('--solve', default=False, action='store_true',
                    help='''After setup, perform the calibration
                    computation by Gauss-Newton iterations.'''
                    )
parser.add_argument('--eliminate', type=float, default=-1.0,
                     help='''The threshold in quantities of model
                     error standard deviations for outlier
                     elimination.'''
)
args = parser.parse_args()
# args=parser.parse_args(['--face_ids','west','north'])

# Specification of parameters for calibration
id_par_spec = collections.OrderedDict([
    ['qe0', np.array([False, True, True, True, True, False])],
    ['cyl_rolls', True],
    ['origo_sens', True],
    ['dir_sens', True],
    ['plane', True]])
# sample_set = '0.005'
# face_ids = ['west','north']


rob_def = UR5_ML_CylRolls()

par_noise = 0.01

nominal_offsets = np.zeros(6, dtype=np.float64)
nominal_cyl_rolls = np.zeros(2, dtype=np.float64)
if args.run_type == 'emu':
    real_offsets = np.loadtxt(
        _setup_folder +
        'emu_joint_offset_vector.npy.txt')
    cal_offsets = real_offsets + np.random.uniform(-par_noise, par_noise, 6)
    # # For testing purposes, set the unidentified encoder offsets to
    # # nominal values
    inv_id_qe0 = np.negative(id_par_spec['qe0'])
    rob_def.encoder_offsets[inv_id_qe0] = real_offsets[inv_id_qe0]
    real_cyl_rolls = np.loadtxt(
        _setup_folder +
        'emu_cylinder_roll_vector.npy.txt')
    if id_par_spec['cyl_rolls']:
        cal_cyl_rolls = real_cyl_rolls + np.random.uniform(-par_noise, par_noise, 2)
    else:
        cal_cyl_rolls = real_cyl_rolls.copy()
else:
    cal_offsets = nominal_offsets.copy()
    cal_cyl_rolls = nominal_cyl_rolls.copy()

rob_def.encoder_offsets = cal_offsets
rob_def.cylinder_rolls = cal_cyl_rolls


rob_base_pose = m3d.Transform(
    np.loadtxt(os.path.join(
        _setup_folder, 'rob_base_pose.npy.txt')))

qenc_dist_sample_sets = [np.load(
    os.path.join(
        _data_folder, args.sample_sets_id,
        'sample_{}_{}.npy'.format(args.run_type, face_id)))
    for face_id in args.face_ids]
n_sample_sets = len(qenc_dist_sample_sets)

cal_faces = [pickle.load(
    open(os.path.join(_setup_folder,
                      '{}.cal_face.pickle'.format(face_id)), 'rb'))
    for face_id in args.face_ids]
nominal_planes = [Plane(pn_pair=(cf.origo, cf.normal))
               for cf in cal_faces]
# # Set up some planes for calibration which have sligth perturbations
# # added.
cal_planes = [Plane(pn_pair=(cf.origo, cf.normal))
              for cf in cal_faces]
if args.run_type == 'emu':
    cal_plane_perturbations = [
        m3d.Vector(-1, -2, 2).normalized,
        m3d.Vector(1, -2, 2).normalized,
        m3d.Vector(-1, 1, -2).normalized]
    if id_par_spec['plane']:
        for i in range(len(cal_planes)):
            cal_planes[i].plane_vector = (
                cal_planes[i].plane_vector
                + par_noise * cal_planes[i].plane_vector.length
                * cal_plane_perturbations[i])

lds_par_vec_nominal = np.loadtxt(
    os.path.join(_setup_folder,
                 'laser_sensor_parameters.npy.txt'))
lds_par_vec = lds_par_vec_nominal.copy()
if args.run_type == 'emu':
    if id_par_spec['origo_sens']:
        lds_par_vec[:3] += np.random.uniform(-par_noise, par_noise, 3)
    if id_par_spec['dir_sens']:
        lds_par_vec[3:] += np.random.uniform(-par_noise, par_noise, 2)

lds_ext_par = LDSExtPar(lds_par_vec)

# // Debug stuff
np.set_printoptions(linewidth=150, precision=5)
#pcw,pcn = lds_rob_cal._plane_computers
#self._update_model()


def max_err():
    return np.max(np.abs(lds_rob_cal._model_errors))


def q0e_err():
    if args.run_type == 'emu':
        return real_offsets - rob_def.encoder_offsets
    else:
        return -rob_def.encoder_offsets


def lds_err():
    return lds_par_vec_nominal - lds_par_vec


def plane_err():
    return [rp.plane_vector-cp.plane_vector
            for rp, cp in zip(nominal_planes, cal_planes)]


def errs():
    print('Max-dist-err {}'.format(max_err()))
    if args.run_type == 'emu':
        print('Offset-err {}'.format(q0e_err()))
        print('LDS-err {}'.format(lds_err()))
        print('Plane-err {}'.format(plane_err()))


def validate(sample_sets):
    pcs = lds_rob_cal._plane_computers
    merrs = []
    for i in range(len(pcs)):
        merrs += [pcs[i].compute_model_error(qd) for qd in sample_sets[i]]
    merrs = np.array(merrs)
    print('Max validation error over {} samples: {}'.format(
        sum([vss.size for vss in sample_sets]),
        np.max(np.abs(merrs))))
    return merrs
# val_model_errs = validate()


def save_cal(prefix='cal_pars', sets_id=''):
    cal_folder = os.path.join(_data_folder, sets_id, 'calibration')
    if not os.path.isdir(cal_folder):
        os.mkdir(cal_folder)
    np.savetxt(
        os.path.join(cal_folder,
                     prefix+'_offsets.npy.txt',
                     ),
        rob_def.encoder_offsets)
    np.savetxt(
        os.path.join(cal_folder,
                     prefix+'_cyl_rolls.npy.txt',
                     ),
        rob_def.cylinder_rolls)
    np.savetxt(
        os.path.join(cal_folder,
                     prefix+'_lds_ext.npy.txt',
                     ),
        lds_ext_par.par_vec)
    


def select_n_first(n):
    n_per_set = n // n_sample_sets
    cal_sample_sets = [ss[:n_per_set] for ss in qenc_dist_sample_sets]
    val_sample_sets = [ss[n_per_set:] for ss in qenc_dist_sample_sets]
    return (cal_sample_sets, val_sample_sets)


def select_n_random(n):
    n_per_set = n // n_sample_sets
    cal_sample_sets = []
    val_sample_sets = []
    for ss in qenc_dist_sample_sets:
        p = np.random.permutation(ss.size)
        cal_sample_sets.append(ss[p[:n_per_set]])
        val_sample_sets.append(ss[p[n_per_set:]])
    return (cal_sample_sets, val_sample_sets)


def detmax_bf_run(inter_max_count_limit=50, quiet=False, silent=False):
    detmax = 0
    detmax_sample_sets = None
    detmax_val_sample_sets = None
    inter_max_count = 0
    while inter_max_count < inter_max_count_limit:
        cal_sample_sets, val_sample_sets = select_n_random(args.n_cal)
        lds_rob_cal.set_sample_sets(cal_sample_sets, update=True)
        det = lds_rob_cal.determinant
        max_mark = ''
        if det > detmax:
            detmax = det
            detmax_sample_sets = cal_sample_sets
            detmax_val_sample_sets = val_sample_sets
            max_mark = ' !!! '
            inter_max_count = 0
        else:
            inter_max_count += 1
        if not quiet:
            print('Regressor determinant: {}{}'.format(det, max_mark))
    if not silent:
        print('Maximal regressor determinant: {}'.format(detmax))
    lds_rob_cal.set_sample_sets(detmax_sample_sets)
    lds_rob_cal.gauss_newton_solve()
    v = validate(detmax_val_sample_sets)
    errs()
    save_cal(prefix='detmax_' + '+'.join(args.face_ids) + '_{}'.format(args.n_cal),
             sets_id=args.sample_sets_id)


def plot_model_errs(error_set=None):
    from matplotlib import pyplot as pp
    if error_set is None:
        me = lds_rob_cal.model_errors
    else:
        me = error_set
    std = np.std(me)
    avg = np.average(me)
    pp.hist(me, bins=len(me)/10, color='b')
    pp.axvline(avg, color='g')
    pp.axvline(avg + 2*std, color='r')
    pp.axvline(avg - 2*std, color='r')
    pp.show(block=False)


def upd_plot(eliminate=False):
    lds_rob_cal.full_update()
    if eliminate:
        lds_rob_cal.eliminate_outliers()
        lds_rob_cal.update_model()
        lds_rob_cal.update_model_errors()
    print(lds_rob_cal.model_max_error)
    plot_model_errs()


lds_rob_cal = LDSRobCal(rob_def, rob_base_pose,
                        cal_planes, lds_ext_par,
                        identify_parameters=id_par_spec)

# // Init
# lds_rob_cal.set_sample_sets(qenc_dist_sample_sets)
cal_sample_sets, val_sample_sets = select_n_random(args.n_cal)
lds_rob_cal.set_sample_sets(cal_sample_sets)
lds_rob_cal.update_model()
lds_rob_cal.update_model_errors()
if args.solve:
    lds_rob_cal.gauss_newton_solve(eliminate_threshold=args.eliminate)
    v = validate(val_sample_sets)
    save_cal(prefix='+'.join(args.face_ids) + '_{}'.format(args.n_cal),
             sets_id=args.sample_sets_id)
    print('Identified cylinder rolls:',rob_def.cylinder_rolls)
import __main__
if hasattr(__main__, '__file__'):
    import code
    code.interact(banner='', local=globals())
