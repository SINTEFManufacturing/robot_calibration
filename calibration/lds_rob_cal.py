"""
Module for the overall (aggregate) class for calibration analysis.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import collections

import math3d as m3d
import numpy as np

from pymoco.kinematics.framecomputer import FrameComputer

from lds_rob_plane_comp import LDSRobPlaneComp

class LDSRobCal(object):
    """Calibration class for creating and aggregating several
    plane-specific computation classes, and computing the grand
    calibration."""

    def __init__(self, rob_def, rob_base_pose,
                 cal_planes, lds_ext_par, sample_sets=None,
                 identify_parameters=collections.OrderedDict(
            [['qe0', np.array(6*[True])],
             ['cyl_rolls', True],
             ['origo_sens', True],
             ['dir_sens', True],
             ['plane', True]])):
        self.rob_def = rob_def
        self.rob_base_pose = rob_base_pose
        self._cal_planes = cal_planes
        self.lds_ext_par = lds_ext_par
        self.id_pars = identify_parameters
        self._n_sets = len(self._cal_planes)
        self.frame_comp = FrameComputer(rob_def=self.rob_def,
                                        cache_in_frames=True)
        # Accounting for the placement of various Jacobians in the
        # total regressor
        self.id_pars_indices = dict()
        index = 0
        # Joint encoder offsets
        if np.any(self.id_pars['qe0']):
            self.n_qe0 = np.count_nonzero(self.id_pars['qe0'])
            self.id_pars_indices['qe0'] = np.arange(
                index, index + self.n_qe0, dtype=np.uint8)
            index += self.n_qe0
        # Cylinder rolls
        if self.id_pars['cyl_rolls']:
            self.id_pars_indices['cyl_rolls'] = np.arange(
                index, index+2, dtype=np.uint8)
            index += 2
        # LDS parameters
        if self.id_pars['origo_sens']:
            self.id_pars_indices['origo_sens'] = np.arange(
                index, index+3, dtype=np.uint8)
            index += 3
        if self.id_pars['dir_sens']:
            self.id_pars_indices['dir_sens'] = np.arange(
                index, index+2, dtype=np.uint8)
            index += 2
        # Planes
        self._n_non_plane_pars = index
        self._n_pars = index
        if self.id_pars['plane']:
            self.id_pars_indices['plane'] = np.arange(
                index, index+3, dtype=np.uint8)
            index += 3
            self._n_pars += 3 * len(self._cal_planes)
        self.par_group_switches = np.array(
            [np.any(pspec) for pspec in list(self.id_pars.values())])
        self._plane_computers = [
            LDSRobPlaneComp(self, cp)
            for cp in cal_planes]
        if not sample_sets is None:
            self.set_sample_sets(sample_sets)

    def set_sample_sets(self, sample_sets, update=False):
        """Set a new sample sets and optionally perform a model
        update."""
        self._qenc_dist_sample_sets = sample_sets
        self._n_samples = np.sum([
            len(ss) for ss in self._qenc_dist_sample_sets])
        [pc.set_samples(ss) for pc, ss in
         zip(self._plane_computers, self._qenc_dist_sample_sets)]
        if update:
            self.update_model()
            self.update_regressor()

    def update_sample_sets(self):
        """Update the sample sets from the sets held by the plane
        computers."""
        self._qenc_dist_sample_sets = [
            pc._enc_dist_samples for pc in self._plane_computers]
        self._n_samples = np.sum([
            len(ss) for ss in self._qenc_dist_sample_sets])

    def update_regressor(self):
        """Stack a total regressor with offsetting the plane
        correction blocks. This assumes that the model of the plane
        computers have been updated."""
        self._dmd_dpar = np.zeros((self._n_samples, self._n_pars))
        plane_index = 0
        row_start = 0
        for pc in self._plane_computers:
            plane_dmd_dpar = pc.dmd_dpar
            n_samples = len(plane_dmd_dpar)
            row_end = row_start + n_samples
            if self.id_pars['plane']:
                plane_start_col = self._n_non_plane_pars + plane_index * 3
                plane_end_col = plane_start_col + 3
                self._dmd_dpar[
                    row_start:row_end, :self._n_non_plane_pars
                    ] = plane_dmd_dpar[:, :self._n_non_plane_pars]
                self._dmd_dpar[
                    row_start:row_end, plane_start_col:plane_end_col
                    ] = plane_dmd_dpar[:, self._n_non_plane_pars:]
                plane_index += 1
            else:
                self._dmd_dpar[row_start:row_end, :] = plane_dmd_dpar
            row_start = row_end

    def update_model_errors(self):
        """The model (residual) errors can be computed on updated
        plane computers."""
        self._model_errors = np.empty(self._n_samples)
        plane_index = 0
        row_start = 0
        for pc in self._plane_computers:
            plane_dmd_dpar = pc.dmd_dpar
            n_samples = len(plane_dmd_dpar)
            row_end = row_start + n_samples
            self._model_errors[row_start:row_end] = pc.model_errors
            row_start = row_end

    def compute_correction(self):
        """Compute the parameter corrections based on the current
        regressor. This typically follows an update to the
        regressor."""
        self._dmd_dpar_inv = np.linalg.pinv(self._dmd_dpar)
        self._corr = self._dmd_dpar_inv.dot(self._model_errors)

    @property
    def svd(self):
        """The SVD of the current regressor."""
        return np.linalg.svd(self._dmd_dpar)

    @property
    def determinant(self):
        """The determinant of the current regressor."""
        a = self._dmd_dpar
        return np.linalg.det(a.T.dot(a))

    def update_model(self):
        """Let all plane computers update their models, typically
        following a parameter correction."""
        for pc in self._plane_computers:
            pc.update()

    def update_parameters(self):
        """Apply the corrections computed by 'compute_corrections'."""
        # Joint encoder offsets
        if np.any(self.id_pars['qe0']):
            qe0_corr = self._corr[self.id_pars_indices['qe0']]
            self.rob_def.encoder_offsets[self.id_pars['qe0']] += qe0_corr
        # Cylinder rolls
        if self.id_pars['cyl_rolls']:
            cr_corr = self._corr[self.id_pars_indices['cyl_rolls']]
            self.rob_def.cylinder_rolls += cr_corr
            self.frame_comp.refresh_link_xforms()
        # LDS parameters
        d_lds_par_vec = np.zeros(5)
        if self.id_pars['origo_sens']:
            os_corr = self._corr[self.id_pars_indices['origo_sens']]
            d_lds_par_vec[:3] = os_corr
        if self.id_pars['dir_sens']:
            phi_corr = self._corr[self.id_pars_indices['dir_sens']]
            d_lds_par_vec[3:] = phi_corr
        self.lds_ext_par.par_vec += d_lds_par_vec
        # Planes
        if self.id_pars['plane']:
            plane_idx = self.id_pars_indices['plane'].copy()
            for i in range(self._n_sets):
                plane_i_corr = self._corr[plane_idx]
                # // Update plane
                self._cal_planes[i].plane_vector = (
                    self._cal_planes[i].plane_vector
                    + m3d.Vector(plane_i_corr))
                plane_idx += 3

    def full_update(self, pre_update=False):
        """Perform a full correct-update cycle, optionally with an
        initially update, for the calibration system."""
        if pre_update:
            self.update_model()
            self.update_model_errors()
        self.update_regressor()
        self.compute_correction()
        self.update_parameters()
        self.update_model()
        self.update_model_errors()

    @property
    def model_errors(self):
        return self._model_errors

    @property
    def model_max_error(self):
        return np.max(np.abs(self._model_errors))

    def eliminate_outliers(self, eliminate_threshold):
        """Delegate elimination of outlier samples to the individual
        plane computers."""
        me = self._model_errors
        std = np.std(me)
        avg = np.average(me)
        elim_stat = [
            pc.eliminate_outliers(eliminate_threshold, avg, std) for pc in self._plane_computers]
        if np.any(elim_stat):
            print('Updating sample sets due to elimination')
            self.update_sample_sets()

    def gauss_newton_solve(self, improvement_limit=1e-10,
                           max_iter=20, eliminate_threshold=4):
        """Based on the stop criterias given as arguments in
        'improvement_limit' and 'max_iter', perform the iterative
        Gauss-Newton solution of the calibration. If 'eliminate' is
        True (default), eliminate outlier samples based on significant
        deviation in error."""
        self.full_update()
        n_iter = 0
        while (np.average(np.abs(self._corr)) > improvement_limit
               and n_iter < max_iter):
            self.full_update()
            if eliminate_threshold>0:
                self.eliminate_outliers(eliminate_threshold)
                self.update_model()
                self.update_model_errors()
            n_iter += 1
        if n_iter > max_iter:
            print('Iteration exit cause : iteration limit!')
        print('Model dist errors: {}m'.format(self.model_max_error))

