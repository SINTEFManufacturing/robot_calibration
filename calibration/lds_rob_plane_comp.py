"""
Module for the support class for computations per calibration plane
for calibration analysis.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import math3d as m3d
import numpy as np

class LDSRobPlaneComp(object):
    """ComputatIon class for simultaneous calibration of laser distance
    sensor pose, robot joint offsets, robot kinematic parameters, and
    one calibration plane."""

    def __init__(self, calibrator, cal_plane, qenc_dist_samples=None):
        self._c = calibrator
        self._lds_ext_par = self._c.lds_ext_par
        self._id_qe0_idx = self._c.id_pars['qe0']
        self._n_qe0 = self._c.n_qe0
        self._par_group_switches = self._c.par_group_switches
        self._cal_plane = cal_plane
        self._rob_def = self._c.rob_def
        self._rob_base_pose = self._c.rob_base_pose
        self._frame_comp = self._c.frame_comp
        ## Declaration of temporaries
        self._q_angs = None
        self._fl_base_poses = None
        self._fl_world_poses = None
        self._df_dqe0_base = None
        self._df_dqe0_world = None
        self._df_dcyl_base = None
        self._df_dcyl_world = None
        self._lds_world_poses = None
        self._lds_base_poses = None
        self._model_dists = None
        self._model_errors = None
        self._dmd_ds_world = None
        self._dmd_dqe0s = None
        self._dmd_dqe0_inv = None
        self._ds_df_base = None
        self._ds_df_world = None
        self._dmd_dphi = None
        self._dmd_dos = None
        self._dmd_dplane = None
        # // Initial sample set
        if not qenc_dist_samples is None:
            self.set_samples(qenc_dist_samples)
        # # // Initial update sequence
        # self.update()

    def set_samples(self, qenc_dist_samples):
        self._n_samples = qenc_dist_samples.size
        self._enc_dist_samples = qenc_dist_samples
        self._q_encs = self._enc_dist_samples['enc']
        self._meas_dists = self._enc_dist_samples['dist']

    def update(self):
        """Execute all updates from the basic sample values in
        '_q_encs' and '_dists'."""
        self._update_q_angs()
        self._update_kinematics()
        # self._update_df_dqe0()
        self._update_lds_poses()
        self._update_model_dists()
        self._update_ds_df()
        self._update_dmd_ds()
        self._update_dmd_dqe0()
        self._update_dmd_dcyl()
        self._update_dmd_dphi()
        self._update_dmd_dos()
        self._update_dmd_dplane()

    def compute_model_dist(self, q_enc):
        """In the current parameter state, compute the model distance
        for the distance sensor. This is not used for calibration, but
        handy for verification."""
        q_ang = self._rob_def.encoder2serial(q_enc)
        fl_base_pose = self._frame_comp(q_ang)
        fl_world_pose = self._rob_base_pose * fl_base_pose
        lds_origo_w = fl_world_pose * self._lds_ext_par.origo
        lds_dir_w = fl_world_pose.orient * self._lds_ext_par.direction
        pv = self._cal_plane.plane_vector
        mod_dist = (1 - pv * lds_origo_w) / (pv * lds_dir_w)
        return mod_dist

    def compute_model_error(self, qenc_dist_sample):
        """Like 'compute_model_dist', but now use the full sample
        data, notably the measured distance sensor reading, to compute
        the model error."""
        model_dist = self.compute_model_dist(qenc_dist_sample['enc'])
        measured_dist = qenc_dist_sample['dist']
        return measured_dist - model_dist

    def eliminate_outliers(self, eliminate_threshold=4.0, avg=None, std=None):
        me = self._model_errors
        # me_abs = np.abs(self._model_errors)
        if std is None:
            std = np.std(me)
        if avg is None:
            avg = np.average(me)
        e_indices = None
        # // Find either low or high outliers
        if me.min() < avg - eliminate_threshold * std:
            e_indices = me.argmin()
            print('Eliminate minimum {} at index {}'.format(
                me.min(), e_indices))
        elif me.max() > avg + eliminate_threshold * std:
            e_indices = me.argmax()
            print('Eliminate maximum {} at index {}'.format(
                me.max(), e_indices))
        if not e_indices is None:
            self.set_samples(np.delete(self._enc_dist_samples, e_indices))
            self.update()
        return not e_indices is None

    def _update_q_angs(self):
        """From the encoder vector, update the joint angles by the
        current state of the robot definition."""
        self._q_angs = np.apply_along_axis(
            self._rob_def.encoder2serial, 1, self._q_encs)

    def _update_kinematics(self):
        """From updated joint angles, compute the corresponding
        kinematic quantities: tool flange poses, cylinder
        roll lines, and encoder jacobian."""
        self._fl_base_poses = np.empty(self._n_samples, dtype='object')
        self._fl_world_poses = np.empty(self._n_samples, dtype='object')
        self._df_dqe0_base = np.empty((self._n_samples, 6, 6), dtype=np.float64)
        self._df_dqe0_world = np.empty((self._n_samples, 6, 6), 
                                       dtype=np.float64)
        self._df_dcyl_base = np.empty((self._n_samples, 6, 2), 
                                      dtype=np.float64)
        self._df_dcyl_world = np.empty((self._n_samples, 6, 2),
                                       dtype=np.float64)
        rob_base_pose = self._rob_base_pose
        # Efficient base to world transform for (jacobian) screws
        b2w = np.zeros((6,6), dtype=np.float64)
        b2w[:3,:3] = rob_base_pose.array[:3,:3]
        b2w[3:,3:] = rob_base_pose.array[:3,:3]
        # Cylinder roll lines
        crl2_infr2, crl3_infr3 = self._rob_def.cylinder_roll_lines
        for i,q_ang in enumerate(self._q_angs):
            # Direct flange pose in base and world
            fbp = self._frame_comp(q_ang)
            self._fl_base_poses[i] = fbp
            fwp = self._rob_base_pose * fbp
            self._fl_world_poses[i] = fwp
            # Arm-Jacobian, i.e. df_dqe0
            df_dqe0_base = self._frame_comp.jacobian()
            self._df_dqe0_base[i] = df_dqe0_base
            df_dqe0_world = np.tensordot(b2w, df_dqe0_base, axes=(1,0))
            self._df_dqe0_world[i] = df_dqe0_world
            # Cylinder Jacobians, i.e. df_dcyl
            infr2_b = self._frame_comp.get_in_frame(n=2)
            crl2_b = np.append(infr2_b * crl2_infr2[0].array, 
                               infr2_b.orient * crl2_infr2[1].array)
            crj2_b = np.append(np.cross(crl2_b[3:], 
                                        fbp.pos.array - crl2_b[:3]),
                               crl2_b[3:])
            crj2_w = np.dot(b2w, crj2_b)
            self._df_dcyl_base[i][:, 0] = crj2_b
            self._df_dcyl_world[i][:, 0] = crj2_w
            infr3_b = self._frame_comp.get_in_frame(n=3)
            crl3_b = np.append(infr3_b * crl3_infr3[0].array, 
                               infr3_b.orient * crl3_infr3[1].array)
            crj3_b = np.append(np.cross(crl3_b[3:], 
                                        fbp.pos.array - crl3_b[:3]),
                               crl3_b[3:])
            crj3_w = np.dot(b2w, crj3_b)
            self._df_dcyl_base[i][:, 1] = crj3_b
            self._df_dcyl_world[i][:, 1] = crj3_w

            
    def _update_df_dqe0(self):
        """Compute the jacobians of the robot flange in base and in
        world."""
        np.array(
            [self._frame_comp.jacobian(q_ang)
             for q_ang in self._q_angs], dtype=np.float64)
        self._df_dqe0_world = np.empty(
            self._df_dqe0_base.shape, dtype=np.float64)
        for si in range(self._n_samples):
            fjb = self._df_dqe0_base[si]
            for i in range(6):
                self._df_dqe0_world[si, :3, i] = (
                    self._rob_base_pose * m3d.Vector(fjb[:3, i])).array
                self._df_dqe0_world[si, 3:, i] = (
                    self._rob_base_pose.orient *
                    m3d.Vector(fjb[3:, i])).array

    def _update_df_dcyl(self):
        """Update the derivative of the robot tool flange with respect
        to the cylinder roll axes. The cylinder axes are given from
        the rob_def in their respective inframes. The inframes given
        in base coordinates are taken from the frame computer, cashed
        during update of the flange poses."""
        

    def _update_lds_poses(self):
        """Compute the LDS poses, as (origo, direction)-pairs in base
        coordinates from the flange poses and the extrinsic sensor
        parameters."""
        self._lds_world_poses = np.empty((self._n_samples, 2), dtype='object')
        self._lds_base_poses = np.empty((self._n_samples, 2), dtype='object')
        lds_origo_fl = self._lds_ext_par.origo
        lds_dir_fl = self._lds_ext_par.direction
        robb_w = self._rob_base_pose
        for i in range(self._n_samples):
            robfl_b = self._fl_base_poses[i]
            lds_origo_b = robfl_b * lds_origo_fl
            lds_dir_b = robfl_b.orient * lds_dir_fl
            self._lds_base_poses[i] = np.array(
                (lds_origo_b, lds_dir_b))
            self._lds_world_poses[i] = np.array(
                (robb_w*lds_origo_b,
                 robb_w.orient*lds_dir_b))

    def _update_model_dists(self):
        """From known sensor poses, the model distances are computed
        from the model of the calibration plane. Also the model errors
        are computed compared to measured distances."""
        mod_dists = []
        mod_errors = []
        pv = self._cal_plane.plane_vector
        for i in range(self._n_samples):
            lds_origo, lds_dir = self._lds_world_poses[i]
            mod_dist = (1 - pv * lds_origo) / (pv * lds_dir)
            mod_dists.append(mod_dist)
            mod_errors.append(self._meas_dists[i]-mod_dist)
        self._model_dists = np.array(mod_dists, dtype=np.float64)
        self._model_errors = np.array(mod_errors, dtype=np.float64)

    def _update_ds_df(self):
        """The derivative of the laser distance sensor pose wrt. to
        the tool flange."""
        self._ds_df_world = np.empty((self._n_samples, 6, 6), dtype=np.float64)
        self._ds_df_base = np.empty((self._n_samples, 6, 6), dtype=np.float64)
        for i in range(self._n_samples):
            # // World
            lds_o_w, lds_d_w = self._lds_world_poses[i]
            dldso_df_w = np.hstack(
                (np.identity(3),
                 -(lds_o_w-self._fl_world_poses[i].pos).cross_operator))
            dldsz_df_w = np.hstack(
                (np.zeros((3, 3)),
                 -lds_d_w.cross_operator))
            self._ds_df_world[i] = np.vstack((dldso_df_w, dldsz_df_w))
            # // Base
            lds_o_b, lds_d_b = self._lds_base_poses[i]
            dldso_df_b = np.hstack(
                (np.identity(3),
                 -(lds_o_b - self._fl_base_poses[i].pos).cross_operator))
            dldsz_df_b = np.hstack(
                (np.zeros((3, 3)),
                 -lds_d_b.cross_operator))
            self._ds_df_base[i] = np.vstack((dldso_df_b, dldsz_df_b))

    def _update_dmd_ds(self):
        """Update and store the derivative of the model distance
        wrt. the laser distance sensor pose. Keep in world
        coordinates."""
        self._dmd_ds_world = np.empty((self._n_samples, 6), dtype=np.float64)
        normal = self._cal_plane.normal
        for i in range(self._n_samples):
            lds_dir = self._lds_world_poses[i][1]
            n_over_lds_proj = (1/(normal * lds_dir) * normal).array
            self._dmd_ds_world[i] = np.append(
                -n_over_lds_proj, -self._model_dists[i] * n_over_lds_proj)

    def _update_dmd_dqe0(self):
        """Compute the derivative of the model distances wrt. the
        robot joint encoder offsets. All quantities are expressed in
        the world coordinate system, since here we have the natural
        definition of the plane."""
        self._dmd_dqe0 = np.empty(
            (self._n_samples, self._n_qe0), dtype=np.float64)
        for i in range(self._n_samples):
            # // dmd_ds
            dmd_ds = self._dmd_ds_world[i]
            # // ds_df
            ds_df = self._ds_df_world[i]
            # // df_dqe0 in world
            df_dqe0_world = self._df_dqe0_world[i]
            # // dmd_dqe0 i
            #dmd_dqe0 = np.dot(dmd_ds
            self._dmd_dqe0[i] = -dmd_ds.dot(ds_df).dot(df_dqe0_world)[
                self._id_qe0_idx]

    def _update_dmd_dcyl(self):
        """Compute the derivative of the model distances wrt. the
        robot cylinder rolls. All quantities are expressed in
        the world coordinate system, since here we have the natural
        definition of the plane."""
        self._dmd_dcyl = np.empty(
            (self._n_samples, 2), dtype=np.float64)
        for i in range(self._n_samples):
            # // dmd_ds
            dmd_ds = self._dmd_ds_world[i]
            # // ds_df
            ds_df = self._ds_df_world[i]
            # // df_dqe0 in world
            df_dcyl_world = self._df_dcyl_world[i]
            # // dmd_dqe0 i
            #dmd_dqe0 = np.dot(dmd_ds
            self._dmd_dcyl[i] = -dmd_ds.dot(ds_df).dot(df_dcyl_world)

    def _update_dmd_dphi(self):
        """Compute the derivative of the model distances wrt. the
        sensor orientation parameters."""
        self._dmd_dphi = np.empty((self._n_samples, 2), dtype=np.float64)
        for i in range(self._n_samples):
            # // dmd_dz
            dmd_dz = self._dmd_ds_world[i, 3:]
            # // dz_dphi ordered with phi_y as first parameter
            fl_orient_world = self._fl_world_poses[i].orient
            phiy, phiz = self._lds_ext_par.phi
            cy = np.cos(phiy)
            cz = np.cos(phiz)
            sy = np.sin(phiy)
            sz = np.sin(phiz)
            dz_dphi_world = np.vstack((
                (fl_orient_world
                 * m3d.Vector(cy * cz, cy * sz, -sy)).array,
                (fl_orient_world
                 * m3d.Vector(-sy * sz, sy * cz, 0)).array)).T
            # // Update
            self._dmd_dphi[i] = dmd_dz.dot(dz_dphi_world)

    def _update_dmd_dos(self):
        """Compute the derivative of the model distances wrt. the
        origo of the sensor. This must be in flange coordinates, which
        is the defining coordinates when considering the sensor origo
        as parameters."""
        self._dmd_dos = np.empty((self._n_samples, 3), dtype=np.float64)
        z_fl = self._lds_ext_par.direction
        n_world = self._cal_plane.normal
        for i in range(self._n_samples):
            # // Transform the calibration face normal to flange coordinates
            n_fl = self._fl_world_poses[i].inverse.orient * n_world
            self._dmd_dos[i] = -n_fl.array / (n_fl * z_fl)

    def _update_dmd_dplane(self):
        """Compute and store the derivative of the model distance with
        respect to the calibration plane, kept in world
        coordinates."""
        self._dmd_dplane = np.empty((self._n_samples, 3), dtype=np.float64)
        # # Calibration plane vector in world coordinates
        n = self._cal_plane.plane_vector
        for i in range(self._n_samples):
            # # Origo and direction of the distance sensor in world
            # # coordinates
            o, z = self._lds_world_poses[i]
            nz = n*z
            # dmd_dplane_vec = (1/(nz**2)) * ((n * o - 1) * z - nz * o)
            dmd_dplane_vec = (1/(nz**2)) * (n.cross(z.cross(o)) - z)
            self._dmd_dplane[i] = dmd_dplane_vec.array

    @property
    def dmd_dpar(self):
        self._dmd_dpar = np.hstack(
            np.array((self._dmd_dqe0,
                      self._dmd_dcyl,
                      self._dmd_dos,
                      self._dmd_dphi,
                      self._dmd_dplane,
                      None
                      ), dtype='object')[self._par_group_switches])
        return self._dmd_dpar

    @property
    def model_errors(self):
        return self._model_errors


