#!/usr/bin/env python3
"""
Application and class module for jogging the robot with sensor-tool
coordinates.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys

sys.path.append('..')

_setup_folder = '../setup/'
_data_folder = '../data/'
_cal_folder = '../calibration/'

import numpy as np
from pymoco.ui.joy_controller import JoyController
# from pymoco.ui.joy_device import JoyDevice
from pymoco.controller_manager import ControllerManager

from lds_par import LDSExtPar


class Jogger(object):
    def __init__(self, rob_host='127.0.0.1', rob_port=5002):
        lds_ext_par_vec = np.loadtxt(
            os.path.join(_setup_folder, 'laser_sensor_parameters.npy.txt'))
        self._lds_ext_par = LDSExtPar(lds_ext_par_vec)
        self._cm = ControllerManager(rob_host=rob_host, 
                               rob_port=rob_port)
        self._cm.tool_xform = self._lds_ext_par.xform
        self._tvc = self._cm.tvc()
        self._jc = JoyController(self._tvc)
        self._jc.start()
        
    def stop(self):
        self._jc.stop(join=True)
        self._cm.stop(join=True)
        

import __main__
if hasattr(__main__, '__file__'):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--rob_host', type=str, default='127.0.0.1')
    parser.add_argument('--rob_port', type=int, default=5002)
    args = parser.parse_args()
    jogger = Jogger(**vars(args))
    import atexit
    atexit.register(jogger.stop)
    import code
    code.interact(banner='', local=globals())

