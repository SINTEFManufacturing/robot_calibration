#!/usr/bin/env python3

"""
Application module for the calibration client application.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import pickle
import socket
import threading
import struct
import time
import atexit
import sys

sys.path.append('..')
_setup_folder = '../setup/'
_data_folder = '../data/'

import math3d as m3d
import numpy as np

sample_dtype = np.dtype([('dist',np.float64), ('enc', np.float64,(6,))])

import pymoco.controller_manager

from lds_par import LDSExtPar, LDSIntPar
from lds_reader import LDSReader
from face_seeker import FaceSeeker
from client_options import cl_args
from target_checker import TargetCheckerClient

sample_dtype = np.dtype([('dist',np.float64), ('enc', np.float64,(6,))])

# pos_limits_x = [0.015,0.05]
# pos_limits_y = [0.2,0.4]
dist_limits = [0.075, 0.085]
yp_lim = np.pi / 8
yaw_limits = [-yp_lim, yp_lim]
pitch_limits = [-yp_lim, yp_lim]
roll_lim = np.pi / 8
roll_limits = [-roll_lim, roll_lim]
angle_limits = [yaw_limits, pitch_limits, roll_limits]

def tbXYZ_to_orient(tb):
    o = m3d.Orientation()
    o.rotate_xt(tb[0])
    o.rotate_yt(tb[1])
    o.rotate_zt(tb[2])
    return o

def goto_pose(pose=None, pos=None, tbXYZ=None):
    """Go linearly with via_*_speed and via_accel to a pose. The pose
    must be give either as an m3d.Transform in 'pose', or as a
    position m3d.Vector in 'pos' AND an orientation as a iterable
    (np.ndarray, m3d.Vector, list, tuple) rpy angle set in 'rpy'."""
    if not pose is None:
        tlc.set_target_pose(pose, 
                            linear_speed=via_lin_speed, 
                            angular_speed=via_ang_speed, 
                            ramp_accel=via_accel)
    elif not pos is None and not tbXYZ is None:
        tlc.set_target_pose(m3d.Transform(tbXYZ_to_orient(tbXYZ), p), 
                            linear_speed=via_lin_speed, 
                            angular_speed=via_ang_speed, 
                            ramp_accel=via_accel)

def get_face_centre_pose(face):
    vec_z = -face.base_pose.orient.vec_z
    vec_y = vec_z.cross(m3d.Vector.e2).normalized
    vec_x = vec_y.cross(vec_z)
    o = m3d.Orientation.new_from_xy(vec_x, vec_y)
    p = (face.base_pose.pos + 
         - np.sum(dist_limits)/2.0 * o.vec_z
         + face.delta_x/2.0 * face.base_pose.orient.vec_x
         + face.delta_y/2.0 * face.base_pose.orient.vec_y)
    return m3d.Transform(o, p)

def go_to_face_centre(face):
    tlc.set_target_pose(get_face_centre_pose(cf_top), linear_speed=0.05, angular_speed=0.2)

def rand_pose(face):
    vec_z = -face.base_pose.orient.vec_z
    vec_y = vec_z.cross(m3d.Vector.e2).normalized
    vec_x = vec_y.cross(vec_z)
    o = m3d.Orientation.new_from_xy(vec_x, vec_y)
    o *= tbXYZ_to_orient([np.random.uniform(*al) for al in angle_limits])
    p = (face.base_pose.pos + 
         - np.random.uniform(*dist_limits) * o.vec_z
         + np.random.uniform(0.0, face.delta_x) * face.base_pose.orient.vec_x
         + np.random.uniform(0.0, face.delta_y) * face.base_pose.orient.vec_y)
    return m3d.Transform(o, p)


def generate_measure_pose(face, wait=True):
    coll = True
    while coll:
        target_pose = rand_pose(face)
        if cl_args.coll_checker:
            coll, q_end = collision_checker.check_arm_tool_path(
                cm.robot_facade.cmd_joint_pos, target_pose)
            if coll:
                print('Collision state {:b}'.format(coll))
                if coll & 1:
                    raise Exception('Inconsistency. Initial state collision!')
        else:
            coll = False 
    if not coll:
        tlc.set_target_pose(target_pose, 
                            linear_speed=via_lin_speed, 
                            angular_speed=via_ang_speed, ramp_accel=via_accel)
        if wait:
            tlc.wait_for_idle()
        return True
    else:
        return False


def sample(face, attempts=1):
    dist = None
    while dist in [None, np.nan] and attempts > 0:
        attempts -= 1
        generate_measure_pose(face)
        dist = ldsr.get_stable_distance()
        if dist in [None, np.nan]:
            print('Distance sample failed: {}'.format(dist))
    if dist in [None, np.nan]:
        return None
    else:
        return dist, robot_definition.serial2encoder(robot_facade.cmd_joint_pos)

def sample_n(face, n_samples=10):
    s_array=np.recarray((n_samples,), sample_dtype)
    si=0
    t0 = time.time()
    while si < n_samples:
        s=sample(face)
        if s is None:
            print('bad sample')
        else:
            s_array[si] = s
            si += 1
            print('Sampled {} of {}'.format(si, n_samples))
    dt=time.time()-t0
    print('Finished sampling. ' 
          + '{} samples in {}s, {} samples per second'.format(n_samples, dt, n_samples/dt))
    return s_array


def store_sample_n(face, name_prefix=None, sub_folder='', n_samples=10, overwrite=False):
    if name_prefix is None:
        name_prefix = 'sample'
    folder = os.path.join(_data_folder, sub_folder)
    if os.path.isdir(folder):
        print('Warning: Writing in existing label sub_folder "{}"'.format(sub_folder))
    else:
        os.mkdir(folder)
    # name_prefix = name_prefix + '_{}'.format(N)
    path_name = os.path.join(folder, '{}.npy'.format(name_prefix))
    s = sample_n(face, n_samples)
    if os.path.isfile(path_name) and not overwrite:
        print('Appending to existing sample file')
        old_samples = np.load(path_name)
        s = np.hstack((old_samples, s))
    else:
        print('Warning: Overwriting a possibly existing sample file')
    np.save(path_name, s)
    print('Stored sample in "{}"'.format(path_name))

def continuous_sample(face):
    while True:
        s=sample(face)
        if s is None:
            print('bad sample')
        else:
            print(s[0])

def retract_sensor(dist=0.01):
    tgt = cm.robot_facade.frame_computer.tool_frame
    tgt.pos += -dist * tgt.orient.vec_z
    tlc.set_target_pose(tgt, linear_speed=0.01)

 

## Start laser sensor reader
ldsr = LDSReader(run=True)
lds_ext_par_vec = np.loadtxt(
    os.path.join(_setup_folder, 'laser_sensor_parameters.npy.txt'))
lds_ext_par = LDSExtPar(lds_ext_par_vec)

## Get interface to robot system
cm = None
if cl_args.run_type == 'real':
    cm = pymoco.controller_manager.ControllerManager(
        rob_host='ur1', log_level=2)
    # // Avoid high speed joint motion
    cm.robot_facade.robot_definition._spd_lim_act = np.pi/4 * np.ones(6)
    # // Use conservative speeds
    via_lin_speed = 0.1
    via_ang_speed = 0.5
    via_accel = 1.0
elif cl_args.run_type == 'emu':
    cm = pymoco.controller_manager.ControllerManager(
        rob_host='127.0.0.1', log_level=1)
    # // Use aggressive speeds
    via_lin_speed = 0.3
    via_ang_speed = 1.5
    via_accel = 5
else:
    print('Need to specify if running "emu" or "real" '
          + 'on the command line ! No Robot !.')


# if not cl_args.coll_event_host is None:
if cl_args.coll_checker:
    collision_checker = TargetCheckerClient((
            cl_args.target_checker_host,cl_args.target_checker_port))

if not cm is None:
    robot_facade = cm.robot_facade
    robot_definition = robot_facade.robot_definition
    atexit.register(cm.stop)
    cm.tool_xform = lds_ext_par.xform
    if cl_args.coll_checker:
        collision_checker.set_tool_xform(lds_ext_par.xform)
    tlc = cm.tlc(linear_speed=via_lin_speed, angular_speed=via_ang_speed)

rob_base_in_world = m3d.Transform(
    np.loadtxt(os.path.join(_setup_folder, 'rob_base_pose.npy.txt')))

cal_faces = {}
for cf_name in ['west', 'north', 'top']:
    cf = pickle.load(
        open(os.path.join(_setup_folder, '{}.cal_face.pickle'.format(cf_name)), 'rb'))
    cf.base_pose = rob_base_in_world.inverse * cf.pose
    cal_faces[cf_name] = cf
    globals()['cf_{}'.format(cf_name)] = cf

# cf_west = pickle.load(
#     open(os.path.join(_setup_folder, 'west.cal_face.pickle'), 'rb'))
# cf_west.base_pose = rob_base_in_world.inverse * cf_west.pose
# cf_north = pickle.load(
#     open(os.path.join(_setup_folder, 'north.cal_face.pickle'), 'rb'))
# cf_north.base_pose = rob_base_in_world.inverse * cf_north.pose
# cf_top = pickle.load(
#     open(os.path.join(_setup_folder, 'top.cal_face.pickle'), 'rb'))
# cf_top.base_pose = rob_base_in_world.inverse * cf_top.pose
# name2face = {'west':cf_west, 'north':cf_north, 'top':cf_top}

# if not cm is None:
#     wfs = FaceSeeker(cm, ldsr, cf_west)

def start():
    store_sample_n(cal_faces[cl_args.face_id],
                   name_prefix=('sample_' + 
                                '{self.run_type}_' + 
                                '{self.face_id}'
                                ).format(self=cl_args),
                   sub_folder = cl_args.sample_sets_name,
                   n_samples=cl_args.n_samples, 
                   overwrite=cl_args.overwrite)

if cl_args.start:
    start()

import code
code.interact(banner='', local=globals())

# store_sample_n('sample_test_0.000',N=200)
# store_sample_n('sample_test_0.001',N=200)
# store_sample_n('sample_test_0.005',N=200)


