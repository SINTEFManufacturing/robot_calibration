import os
import sys

import math3d as m3d
import numpy as np

file_dir = os.path.split(__file__)[0]
sys.path.append(os.path.join(file_dir, '../..'))

from calibration.plane import Plane
from lds_par import LDSExtPar, LDSIntPar

sample_dtype = np.dtype([('dist',np.float64), ('enc', np.float64,(6,))])

setup_folder = os.path.join(file_dir, '../../setup/')
data_folder = os.path.join(file_dir, '../../data/')
cal_folder = os.path.join(file_dir, '../../calibration/')

fid2folder = dict(c=cal_folder,d=data_folder,s=setup_folder)
