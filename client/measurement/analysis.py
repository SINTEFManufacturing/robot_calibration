from __init__ import *

from pymoco.kinematics.framecomputer import FrameComputer
from pymoco.robot import create_robot


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--project_name', default='', 
                    help='''Project name for retrieving calibrated
                    parameters and storing measurements. This should
                    be a folder name under the "../data" folder.'''
                    )
parser.add_argument('--calibration_prefix', default='', 
                    help='''The prefix for the calibration parameters
                    such that, say,
                    "../data/<project_name>/<calibration_prefix>_offsets.npy.txt"
                    is a valid file name.'''
)
args = parser.parse_args()

if args.project_name != '':
    proj_folder = os.path.join(data_folder, args.project_name)
    enc_offs =  np.loadtxt(os.path.join(proj_folder, args.calibration_prefix + '_offsets.npy.txt'))
    print('Calibrated encoder offsets: {}'.format(enc_offs))
    lds_ext_par_vec = np.loadtxt(os.path.join(proj_folder, args.calibration_prefix + '_lds_ext.npy.txt'))
    print('Calibrated LDS-parameters : {}'.format(lds_ext_par_vec))
    cyl_rolls = np.loadtxt(os.path.join(proj_folder, args.calibration_prefix + '_cyl_rolls.npy.txt'))
    print('Calibrated cylinder rolls : {}'.format(cyl_rolls))
else:
    enc_offs =  np.zeros(6, dtype=np.float64)
    print('Nominal encoder offsets: {}'.format(enc_offs))
    cyl_rolls =  np.zeros(2, dtype=np.float64)
    print('Nominal cylinder rolls : {}'.format(cyl_rolls))
    lds_ext_par_vec = np.loadtxt(
        os.path.join(setup_folder, 'laser_sensor_parameters.npy.txt'))
    print('Nominal LDS-parameters : {}'.format(lds_ext_par_vec))
    
lds_ext_par = LDSExtPar(lds_ext_par_vec)
rob_def = create_robot('UR5_ML_CR')
rob_def.cylinder_rolls = cyl_rolls
fc = FrameComputer(tool_xform=lds_ext_par.xform, 
                   rob_def=rob_def)

def point_from_dq(d, q):
    sf = fc(rob_def.encoder2serial(q))
    return sf.pos + d * sf.orient.vec_z

def planes_from_points(face1_pts, face2_pts):
    plane1 = Plane(points=face1_pts)
    plane2 = Plane(points=face2_pts)
    print('Plane normals dot product error = {:.6e}'.format(1-np.abs(plane1.normal*plane2.normal)))
    print('Reference point1 distance to plane2 = {:.6e}'.format(plane2.dist(plane1.point)))
    print('Reference point2 distance to plane1 = {:.6e}'.format(plane1.dist(plane2.point)))
    return plane1,plane2

def planes_from_dq_samples(dqs0, dqs1):
    pts0 = np.array([point_from_dq(d,q).array for d,q in dqs0])
    pts1 = np.array([point_from_dq(d,q).array for d,q in dqs1])
    return planes_from_points(pts0, pts1)

fdq0=np.load(os.path.join(proj_folder,'face_sample_0.npy'))
fdq1=np.load(os.path.join(proj_folder,'face_sample_1.npy'))

# Manually optimized
#rob_def.cylinder_rolls=[0.015,-0.02525]
#fc = FrameComputer(tool_xform=lds_ext_par.xform, 
#                   rob_def=rob_def)
pl0,pl1 = planes_from_dq_samples(fdq0, fdq1)
