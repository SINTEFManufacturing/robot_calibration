#!/usr/bin/env python3
"""
Application module for jogging the robot and sampling positions.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys

import math3d as m3d
import numpy as np


from __init__ import (setup_folder, data_folder, cal_folder, 
                      sample_dtype, Plane, fid2folder)
from analysis import planes_from_points

# def getfn(name, fid, sub_folders=[]):
#     segments = [fid2folder[fid]] + sub_folders + [name]
#     return os.path.join(*segments) 

import numpy as np
from pymoco.ui.joy_controller import JoyController
from pymoco.ui.joy_device import JoyDevice
from pymoco.controller_manager import ControllerManager
from pymoco.kinematics.framecomputer import FrameComputer
from pymoco.robot import create_robot

from lds_reader import LDSReader


import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('--run_type', str, default='emu', choices=['emu','real'])
parser.add_argument('--offline', default=False, action='store_true',
                    help="""The default is live mode. If set, no
                    connection to the robot is attempted, and no
                    joystick control is set up."""
)
parser.add_argument('--rob_host', type=str, default='127.0.0.1')
parser.add_argument('--rob_port', type=int, default=5002)
parser.add_argument('--project_name', default='', 
                    help='''Project name for retrieving calibrated
                    parameters and storing measurements. This should
                    be a folder name under the "../data" folder.'''
                    )
parser.add_argument('--calibration_prefix', default='', 
                    help='''The prefix for the calibration parameters
                    such that, say,
                    "../data/<project_name>/<calibration_prefix>_offsets.npy.txt"
                    is a valid file name.'''
)
args = parser.parse_args()

if args.project_name != '':
    proj_folder = os.path.join(data_folder, args.project_name)
    enc_offs =  np.loadtxt(os.path.join(proj_folder, args.calibration_prefix + '_offsets.npy.txt'))
    print('Calibrated encoder offsets: {}'.format(enc_offs))
    lds_ext_par_vec = np.loadtxt(os.path.join(proj_folder, args.calibration_prefix + '_lds_ext.npy.txt'))
    print('Calibrated LDS-parameters : {}'.format(lds_ext_par_vec))
else:
    enc_offs =  np.zeros(6, dtype=np.float64)
    print('Nominal encoder offsets: {}'.format(enc_offs))
    lds_ext_par_vec = np.loadtxt(
        os.path.join(setup_folder, 'laser_sensor_parameters.npy.txt'))
    print('Nominal LDS-parameters : {}'.format(lds_ext_par_vec))

lds_ext_par = LDSExtPar(lds_ext_par_vec)

if not args.offline:
    cm = ControllerManager(rob_host=args.rob_host, 
                           rob_port=args.rob_port, 
                           encoder_offsets=enc_offs)
    cm.tool_xform = lds_ext_par.xform
    rob_fac = cm.robot_facade
    rob_def = rob_fac.robot_definition
    tvc = cm.tvc()
    jc = JoyController(tvc)
    jdev = jc.jdev
    jc.start()
    ldsr = LDSReader(run=True)
else:
    rob_def = pymoco.robot.create_robot('UR_ML_CR')

fc = FrameComputer(tool_xform=lds_ext_par.xform, 
                   rob_def=rob_def)


def sample(savename=None):
    sample_points = []
    while True:
        jdev.wait_for_button_event()
        if jdev.button_event == (3,1):
            # // Test for valid sample
            print('Testing sample ... ')
            print('\t ... result : {}'.format(ldsr.get_stable_distance()))
        if jdev.button_event == (7,1):
            # // Sample and store if valid
            print('Sampling point to set ... ')
            d = ldsr.get_stable_distance()
            sf = fc(cm.robot_facade.act_joint_pos)
            if d in [None, np.nan]:
                print('\t... No measurement! ({})'.format(d))
            else:
                p = sf.pos + d * sf.orient.vec_z
                sample_points.append(p.array)
                print('\t... {: 7.5f} {}'.format(d,p))
        elif jdev.button_event == (8,1):
            # // Quit
            print('Quitting!')
            sample_set = np.array(sample_points)
            if not savename is None:
                np.save(savename,sample_set)
            return sample_set

def sample_planes():
    pts1=sample('face1')
    pts2=sample('face2')
    pl1,pl2 = planes_from_points(pts1,pts2)
    return pl1,pl2


face_number = 0
def sample_face_points(n=4, radius=0.01, save=True, pre_centre=True):
    """Sample 'n' points on a circle of 'radius', normal to the current sensor
    direction.
    """
    global face_number
    init_pose = cm.robot_facade.frame_computer()
    # // Take over control from the joystick
    print('Suspending joystick control')
    jc.suspend()
    tlc = cm.tlc(linear_speed=0.03, angular_speed=0.1, ramp_accel=0.5)
    # Pre-adjust to centre of measuring range
    if pre_centre:
        dm = ldsr.get_stable_distance()
        d_move_tz = dm - ldsr._model.centre_dist
        t_centre = m3d.Transform()
        t_centre.pos.z = d_move_tz
        print('Centering (dz={})'.format(d_move_tz))
        tlc.set_target_pose(t_centre, express_frame='Tool')
        tlc.wait_for_idle()
        init_pose = cm.robot_facade.frame_computer()
    # // Generate poses
    sposes = []
    for i in range(n):
        p = init_pose.copy()
        rot = init_pose.orient.copy()
        rot.rotate_z(i * 2 * np.pi / n)
        p.pos += radius * rot.vec_x
        sposes.append(p)
    # // Perform sampling
    print('Starting measurements.')
    p_samples = np.empty((n,3), dtype=np.float64)
    qd_samples = np.recarray(n, dtype=sample_dtype)
    i = 0
    for p in sposes:
        tlc.set_target_pose(p)
        print('Moving to pose: {}'.format(p.pos))
        tlc.wait_for_idle()
        d = ldsr.get_stable_distance()
        if d in [None, np.nan]:
            print('Got no measurement')
            # // Return to start pose
            tlc.set_target_pose(pose)
            return None
        else:
            p_samples[i] = (p.pos + d * p.orient.vec_z).array
            qd_samples['dist'][i] = d
            qd_samples['enc'][i] = rob_def.serial2encoder(rob_fac.cmd_joint_pos)
        i += 1
    # // Return to pose
    tlc.set_target_pose(init_pose)
    print('Returning to origin')
    tlc.wait_for_idle()
    # // Resume joystick controller and return sampled points
    print('Resume joystick control')
    jc.resume(cm.tvc())
    if save:
        np.save(os.path.join(proj_folder, 'face_sample_{}.npy'.format(face_number)), qd_samples)
        face_number += 1
    return p_samples, qd_samples


import code
code.interact(banner='', local=globals())

