#!/usr/bin/env python3
"""
Module for the laser distance sensor reader class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import socket
import threading
import struct
import time

import numpy as np

sys.path.append('..')

from lds_par import LDSIntPar


class _RingBuffer(np.ndarray):
    def __new__(cls, size):
        return np.ndarray.__new__(cls, (size,), dtype=np.float64)
    def __init__(self, size):
        self[:] = 0.0
        self._size = size
        self._i = 0
    def __iadd__(self, val):
        self[self._i] = val
        self._i = (self._i + 1) % self._size
        return self
    @property
    def current(self):
        return self[self._i]
    @property
    def avg(self):
        return np.average(self)
    @property
    def mm_spread(self):
        """Return the max-min spread."""
        return np.float64(self.max() - self.min())
    

class LDSReader(threading.Thread):
    def __init__(self, lds_addr=('127.0.0.1', 4999), stable_mmd=1e-4, stable_time_limit=5.0, logging=False, run=True):
        threading.Thread.__init__(self)
        self.daemon = True
        self._lds_addr = lds_addr
        self._model = LDSIntPar()
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind(self._lds_addr)
        self._voltage = -1.0
        self._read_time = -1.0
        self.__stop = False
        self._logging = logging
        self._oor_cond = threading.Condition()
        self._new_recv_cond = threading.Condition()
        self._stable_mmd = stable_mmd
        self._stable_mmv = self._model._gain * stable_mmd
        self._stable_time_limit = stable_time_limit
        self._sensor_rate = self.__measure_sensor_rate()
        self._buffer = _RingBuffer(int(self._stable_time_limit * self._sensor_rate / 5.0))
        print('LDSReader: Ringbuffer size of {}'.format(self._buffer._size))
        if run:
            self.start()

    def __measure_sensor_rate(self):
        t0=time.time()
        for i in range(10):
            self._socket.recv(1024)
        t1 = time.time()
        return 10.0/(t1-t0)
        
    @property
    def min_max_spread(self):
        return self._buffer.mm_spread

    @property 
    def read_time(self):
        return self._read_time
    
    @property
    def voltage(self):
        return self._voltage
    
    @property
    def new_voltage(self):
        """Blocked read of a newest voltage."""
        with self._new_recv_cond:
            self._new_recv_cond.wait()
        return self._voltage

    @property
    def distance(self):
        return self._model.distance(self._voltage)
    
    @property
    def new_distance(self):
        """Blocked read of a newest voltage, converted to distance."""
        return self._model.distance(self.new_voltage)

    
    def get_stable_distance(self):
        sv = self.get_stable_voltage()
        if sv is None:
            return None
        else:
            return self._model.distance(sv)
        
    def get_stable_voltage(self):
        """Continuously update voltages until variation (max-min) in
        the buffer is bounded by 'self._stable_mmv'."""
        t0 = time.time()
        while (self.min_max_spread > self._stable_mmv
               and time.time()-t0 < self._stable_time_limit):
            time.sleep(0.1)
            #self._new_recv_cond.wait()
        if time.time()-t0 > self._stable_time_limit:
            return None
        else:
            return self._buffer.avg

    @property
    def voltages(self):
        return np.array(self._buffer)
    
    @property
    def distances(self):
        return self._model.distances(self.voltages)

    @property
    def last_in_range_distance(self):
        return self._model.distance(self._last_ir_voltage)

    def wait_for_oor(self, time_out):
        with self._oor_cond:
            return self._oor_cond.wait(time_out)

    def stop(self):
        self.__stop = True

    def run(self):
        while not self.__stop:
            self._last_voltage = self._voltage
            v_pack = self._socket.recv(1024)
            self._read_time = time.time()
            self._voltage = struct.unpack('d', v_pack)[0]
            self._buffer += self._voltage
            with self._new_recv_cond:
                self._new_recv_cond.notify_all()
            if self._voltage > self._model._out_of_range_voltage:
                self._last_ir_voltage = self._last_voltage
                with self._oor_cond:
                    self._oor_cond.notify_all()
            #time.sleep(self._dead_time)
            if self._logging:
                print(self.distance)
        self._socket.close()


if __name__ == '__main__':
    ldsr = LDSReader(logging=False)
    import __main__
    if hasattr(__main__, '__file__'):
        import code
        code.interact(banner='', local=globals())
