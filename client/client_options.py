"""
Module for options to the calibration client system.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import argparse

sysargs = sys.argv[:]
if '--' in sysargs:
    sysargs.remove('--')
parser = argparse.ArgumentParser()
parser.add_argument('--coll_checker', action='store_true', default=False, 
                    help='''Signal if collision checking should be
                    done by communicating with a task checker.'''
                    )
parser.add_argument('--start', action='store_true', default=False, 
                    help='''Signal if sampling should start right away
                    with default setup.'''
                    )
parser.add_argument('--coll_event_host', default='127.0.0.1')
parser.add_argument('--coll_event_port', default=10099)
parser.add_argument('--cal_rob_host', type=str, default='127.0.0.1')
parser.add_argument('--cal_rob_port', type=int, default=5002)
parser.add_argument('--coll_rob_host', type=str, default='127.0.0.1')
parser.add_argument('--coll_rob_port', type=int, default=5003)
parser.add_argument('--face_id', default='west', 
                    help='''The name of the face to sample. The face
                    name must identify a face specification (in pickle
                    format).'''
                    )
parser.add_argument('--sample_sets_name', default='', 
                    help='''The project name referring to a specific
                    collection of sample sets. This will name the
                    sub folder under which samlpes are stored.'''
                    )
parser.add_argument('--overwrite', action='store_true', default=False,
                    help='''If set, an existing sample file is
                    overwritten. Otherwise new samples will be
                    appended.'''
                    )
parser.add_argument('--n_samples', type=int, default=50, 
                    help='''The number of samples to perform in
                    default startup.'''
                    )
parser.add_argument('--run_type', default='emu', choices=['emu','real'], 
                    help='''Set to "emu" or "real". If "emu" some
                    faster motions are allowed and perturbations on
                    parameters are performed to enable debugging.'''
                    )
parser.add_argument('--target_checker_host', default='127.0.0.1', 
                    help='''The host to which the collision checker
                    sends events.'''
                    )
parser.add_argument('--target_checker_port', default=10100, 
                    help='''The port to which the collision checker
                    sends events.'''
                    )
#parser.add_argument('rargs', nargs=argparse.REMAINDER)
cl_args,rargs = parser.parse_known_args(sysargs)
print('Client arguments : ',cl_args)
