"""
Module for face seeker class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d
import numpy as np
import time

class FaceSeeker(object):
    def __init__(self, controller_manager, laser_tool_reader
                 , cal_face, stick_angle=np.pi/6.0, log_level=2):
        self._log_level = log_level
        self._cm = controller_manager
        self._fc = self._cm.robot_facade.frame_computer
        self._rf = self._cm.robot_facade
        self._tlc = self._cm.tlc()
        self._lsr = laser_tool_reader
        self._cal_face = cal_face
        # # The angle at which the sensor measurement direction should
        # # stick while seeking.
        self._stick_angle = stick_angle
        self._seek_speed = 0.0025
        self._sensor_settle_time = 0.5
        dist_lims = self._lsr._model._dist_limits
        self._sensor_min_dist = dist_lims[0]
        self._sensor_max_dist = dist_lims[1]
        self._sensor_centre_dist = (dist_lims[0] + 
                                    0.5 * (dist_lims[1] - dist_lims[0]))
        self._cal_face_found_in_base = None
        

    # @property
    # def face_in_base(self):
    #     if not self._cal_face_found_in_base is None:
    #         return self._cal_face_found_in_base
    #     else:
            
    def _log(self, message, log_level=2):
        if log_level < self._log_level:
            print('FaceSeeker({}) : {}'.format(
                    self._cal_face.name, str(message)))

    # def seek_step(self, init_pose, direction, length):
    #     """Seek a sensor step by moving 'length' in 'direction' from
                                    #     'init_pose'."""

    def seek_to_dist(self, seek_direction, target_distance, 
                     max_seek_length=0.1, tolerance=0.001):
        """ Seek along 'direction' until the laser distance sensor
        reads 'distance' up to 'tolerance'."""
        current_pose = self._fc.tool_frame
        tgt_pose = current_pose.copy()
        tgt_pose.pos += seek_direction * max_seek_length
        seek_speed = min(0.1, 0.5*tolerance/self._rf.control_period)
        lsr = self._lsr
        self._tlc.wait_for_idle()
        self._tlc.set_target_pose(tgt_pose, linear_speed=seek_speed, ramp_accel=0.5)
        found = False
        while self._tlc.is_running():
            time.sleep(0.01)
            dm = lsr.distance
            if np.isnan(dm):
                self._log('Seek to dist: Not in range.',5)
                continue
            if (dm + tolerance > target_distance
                and dm - tolerance < target_distance):
                self._tlc.abort()
                self._log('Seek to dist: fulfilled',5)
                found = True
                break
            else:
                self._log('Seek to dist: {}'.format(dm),5)
        return found

    def localize_edge(self, edge, edge_location, seek_length=0.06):
        rob_pose = self._fc.tool_frame
        ## Get the current robot approach direction
        rob_a = rob_pose.orient.vec_z
        cf_pose = self._cal_face.base_pose
        ## Determine which edge to seek
        if edge == 'x':
            edge_dir = cf_pose.orient.vec_x
            edge_orth = cf_pose.orient.vec_y
            stick_rotation= m3d.Orientation(-self._stick_angle * edge_dir) 
        else: 
            edge_dir = cf_pose.orient.vec_y
            edge_orth = cf_pose.orient.vec_x 
            stick_rotation= m3d.Orientation(self._stick_angle * edge_dir) 
        ## Target approach with stick angle
        tgt_a = stick_rotation * -cf_pose.orient.vec_z
        ## Setup a transform to use as target.
        tgt = m3d.Transform()
        ## Compute the fastest rotation to correct, normal approach on
        ## the cal_face
        rot = m3d.Orientation.new_vec_to_vec(rob_a, tgt_a)
        tgt.orient = rot * rob_pose.orient
        ## Setup position for starting a seek
        tgt.pos = (cf_pose.pos 
                   - self._sensor_max_dist * tgt_a
                   + seek_length/2.0 * edge_orth
                   + edge_location * edge_dir)
        ## Go to start of first seek
        self._tlc.wait_for_idle()
        self._tlc.set_target_pose(tgt, ramp_accel=0.5)
        self._tlc.wait_for_idle()
        self._log('Moved to seek init pose. (Edge "{}", loc={})'.format(
                edge, edge_location), 3)
        time.sleep(self._sensor_settle_time)
        ## Seek the face to be centred in the laser sensor range here.
        seek_found = self.seek_to_dist(tgt.orient.vec_z, self._sensor_centre_dist)
        if not seek_found:
            self._log('Error: Seek to face unsuccessful. '
                      + '(Edge "{}", loc={})'.format(edge, edge_location), 0)
            return None
        self._tlc.wait_for_idle()
        ## Test that we have a valid measurement
        if np.isnan(self._lsr.distance):
            self._log('Error: No measurement found on initial seek position. '
                      + '(Edge "{}", loc={})'.format(edge, edge_location), 0)
            return None
        ctf = self._fc.tool_frame
        tgt = ctf.copy()
        ## Store a point on the face
        p0 = ctf.pos + self._lsr.distance * ctf.orient.vec_z
        ## Generate seek motion a long -orthogonal of the edge.
        tgt.pos += seek_length * -edge_orth
        self._tlc.set_target_pose(tgt, linear_speed=self._seek_speed, ramp_accel=0.5)
        oor_achieved = self._lsr.wait_for_oor(seek_length / self._seek_speed)
        if not oor_achieved:# not np.isnan(self._lsr.distance):
            self._log('Error: Measurement found on seek end position. ' 
                      + '(Edge "{}", loc={})'.format(edge, edge_location), 0)
            return None
        last_tf = self._cm.robot_facade.frame_computer.tool_frame
        last_dist = self._lsr.last_in_range_distance
        self._tlc.abort()
        self._tlc.wait_for_idle()
        return (p0, last_tf.pos + last_dist * last_tf.orient.vec_z)
    
    def localize_face(self):
        ## Seek the edges
        seek_x0=self.localize_edge(edge='x', edge_location=0.02)
        if seek_x0 is None:
            return
        seek_x1=self.localize_edge(edge='x', edge_location=0.04)
        if seek_x1 is None:
            return
        seek_y0=self.localize_edge(edge='y', edge_location=0.02)
        if seek_y0 is None:
            return
        seek_y1=self.localize_edge(edge='y', edge_location=0.15)
        if seek_y1 is None:
            return
        ## Compute the normal based on cross of point differences on the face
        hat_z = (seek_x1[0]-seek_x0[0]).cross(seek_y1[0]-seek_y0[0]).normalized
        ## Compute x and y directions, removing projections on hat_z
        hat_y=(seek_y1[1]-seek_y0[1])
        hat_y -= (hat_y * hat_z) * hat_z
        hat_y.normalize()
        hat_x=(seek_x1[1]-seek_x0[1])
        hat_x -= (hat_x * hat_z) * hat_z
        hat_x -= (hat_x * hat_y) * hat_y
        hat_x.normalize()
        ## Compute an average origo based on projections between edge points
        origo_x = seek_x1[1]+((seek_y1[1]-seek_x1[1])*hat_x)*hat_x
        origo_y = seek_y1[1]+((seek_x1[1]-seek_y1[1])*hat_y)*hat_y
        origo=origo_x + 0.5 * (origo_y - origo_x)
        ## Estimate a residue error in position
        self._pos_res_err = (origo_y - origo_x).length
        ## Make transform
        self._cal_face_found_in_base = m3d.Transform.new_from_xyp(hat_x, hat_y, origo)
