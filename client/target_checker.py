#!/usr/bin/env python3

"""
Server for collision checking a path to a target tool frame pose. It requires that an emulator have been started with option '--coll_checker' and with some specific robot port, e.g. '--rob_port=5003', or host, e.g. '--rob_host='other_host', avoiding address clash with the live or emulated robot(s) in the system.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import struct
import socket
import time

import math3d as m3d
import numpy as np

import pymoco.controller_manager

from client_options import cl_args
from collision_listener import CollEventListener


task_qp_struct = struct.Struct('6d16d')
task_qq_struct = struct.Struct('6d6d')
task_q_struct = struct.Struct('6d')
task_p_struct = struct.Struct('16d')
task_data_lengths = [s.size for s in 
                     [task_qp_struct, task_q_struct, task_p_struct]]
# class TargetCheckerTask(object):

class TargetChecker(threading.Thread):
    def __init__(
        self, 
        rob_addr=(cl_args.coll_rob_host, cl_args.coll_rob_port), 
        srv_addr=(cl_args.target_checker_host, cl_args.target_checker_port),
        coll_addr=(cl_args.coll_event_host,cl_args.coll_event_port)
        ):
        threading.Thread.__init__(
            self, name='TargetChecker server <{}>'.format(srv_addr))
        self.daemon = True
        self._rob_addr = rob_addr
        self._srv_addr = srv_addr
        self._coll_addr = coll_addr
        # // Get a controller manager
        self._cm = pymoco.controller_manager.ControllerManager(
            rob_type='ur',
            rob_host=self._rob_addr[0],
            rob_port=self._rob_addr[1],
            log_level=1)
        self._cm.robot_facade._scale_commands=False
        self._coll_latency_time = 0.1
        # self._tlc = self._cm.tlc()
        # // Start a server socket
        self._srv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._srv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._srv_sock.bind(self._srv_addr)
        # // Use aggressive speeds
        self._path_motion_spec = dict(
            linear_speed=0.5, angular_speed=2.0, ramp_accel=100.0)
        self._tool_motion_spec = dict(
            linear_speed=5.0, angular_speed=20.0, ramp_accel=100.0)
        # // Start a collision listener
        self._coll_listener = CollEventListener(self._coll_addr)
        self.__stop = False
        self.__started = False

    def _task_qp(self, q, p, cl_addr):
        """Check end points and the linear path between initial arm
        pose 'q' and tool end pose 'p'. The result is sent in a byte
        containing the collision check results and a found solution to
        the target tool pose. The byte will either be 0 for no
        collision, 1 (bit 1 set) for initial pose collision, 2 (bit 2
        set) for path collision, and 2|4 (bits 2 and 3 set) for end
        pose collision."""
        if np.all(np.isnan(q)):
            # // interpret as tool xform
            print('Setting tool transform.')
            self._cm.tool_xform = p
            self._srv_sock.sendto(struct.pack('B',0), cl_addr)
            return
        print('Checking qp path {} -> {}'.format(q,p))
        self._cm.robot_facade.cmd_joint_pos = q
        time.sleep(self._coll_latency_time)
        coll_state = 0 | self._coll_listener.current_collision
        self._coll_listener.clear_accum()
        if not self._coll_listener.current_collision:
            tlc = self._cm.tlc()
            tlc.set_target_pose(p, **self._path_motion_spec)
            # while (not self._coll_listener.accum_collision 
            #        and tlc.is_running()):
            #     time.sleep(self._coll_latency_time)
            # tlc.abort()
            tlc.wait_for_idle()
            end_pose = self._cm.robot_facade.frame_computer(self._cm.robot_facade.cmd_joint_pos)
            if (end_pose.pos.dist(p.pos) > 0.0001 
                or end_pose.orient.ang_dist(p.orient) > 0.0001):
                coll_state |= 1 << 3
            time.sleep(self._coll_latency_time)
            coll_state |= self._coll_listener.current_collision << 2
            coll_state |= (self._coll_listener._coll_accum 
                          > self._coll_listener.current_collision) <<1
            # print('Accum : {}'.format(self._coll_listener._coll_accum))
        print('QP-Task ended with collision state {:b}'.format(coll_state))
        self._srv_sock.sendto(
            struct.pack(
                'B6d', coll_state,
                *self._cm.robot_facade.cmd_joint_pos), 
            cl_addr)
        
    def _task_qq(self, q_start, q_end, cl_addr):
        """Check end points and the linear path between start and end
        arm poses."""
        self._cm.robot_facade.cmd_joint_pos = q_start
        time.sleep(self._coll_latency_time)
        self._coll_listener.clear_accum()
        coll_state = 0
        if not self._coll_listener.current_collision:
            jlc = self._cm.jlc()
            jlc.set_joint_target(q_end)
            while (not self._coll_listener.accum_collision 
                   and jlc.is_running()):
                time.sleep(self._coll_latency_time)
            jlc.abort()
            print('Accum : {}'.format(self._coll_listener._coll_accum))
            if self._coll_listener.accum_collision:
                print('Task ended on collision')
        self._srv_sock.sendto(
            struct.pack('B',self._coll_listener.accum_collision), 
            cl_addr)

    def _task_q(self, q, cl_addr):
        """Check the given arm pose for collision."""
        self._cm.robot_facade.cmd_joint_pos = q_start
        time.sleep(self._coll_latency_time)
        self._srv_sock.sendto(
            struct.pack('B',self._coll_listener.current_collision), 
            cl_addr)
      
    def _task_p(self, p, cl_addr):
        """Check tool pose for collisions, and send the found inverse
        kinematics solution if collision free."""
        self._cm.robot_facade.cmd_joint_pos = q
        time.sleep(self._coll_latency_time)
        tlc = self._cm.tlc()
        tlc.set_target_pose(p, **self._tool_motion_spec)
        time.sleep(self._coll_latency_time)
        self._srv_sock.sendto(
            struct.pack(
                'B6d',self._coll_listener.current_collision,
                *self._cm.robot_facade.cmd_joint_pos), 
            cl_addr)



    def run(self):
        self._coll_listener.start()
        self.__started = True
        while not self.__stop:
            print('TC: Waiting for task')
            pkg = self._srv_sock.recvfrom(4096)
            data, addr = pkg
            data_length = len(data)
            if data == b'quit':
                print('TC: "quit" receivet.')
            elif not data_length in task_data_lengths:
                print('TC: Received packet of wrong size. '
                      + 'Received {} byte(s), but need a size in {}.'
                      .format(data_length, task_data_lengths))
                self._srv_sock.sendto(
                    b'error: unreckognized task data lenght', addr)
                continue
            if data_length == task_qp_struct.size:
                # // Arm pose - path - tool pose check
                qp = np.array(task_qp_struct.unpack(data))
                q = qp[:6]
                p = m3d.Transform(qp[6:].reshape((4,4)))
                self._task_qp(q, p, addr)
            elif data_length == task_qq_struct.size:
                # // Arm pose - path - arm pose check
                qq = np.array(task_qq_struct.unpack(data))
                q_start = qq[:6]
                q_end = qq[6:]
                self._task_qq(q_start,q_end, addr)
            elif data_length == task_q_struct.size:
                # // Arm pose check
                self._task_q(np.array(task_q_struct.unpack(data)), addr)
            elif data_length == task_p_struct.size:
                # // Tool pose check
                p_data = np.array(task_p_struct.unpack(data)).reshape((4,4))
                p = m3d.Transform(p)
                self._task_p(p, addr)
        self._coll_listener.stop()

    def stop(self):
        self.__stop = True
        self._srv_sock.sendto(b'quit', self._srv_addr)
        self.join()
        print('Stopped')

class TargetCheckerClient(object):
    def __init__(self, tc_addr=(cl_args.target_checker_host, cl_args.target_checker_port)):
        self._tc_addr = tc_addr
        self._tc_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._tc_sock.settimeout(2)

    def _invoke_task(self, pkg):
        reply_received = False
        while not reply_received:
            try:
                self._tc_sock.sendto(pkg, self._tc_addr)
                reply = self._tc_sock.recv(1024)
            except socket.timeout:
                print('Failed to get reply within timout. Retrying.')
            else:
                return reply
    
    def set_tool_xform(self, tool_xform):
        pkg = task_qp_struct.pack(*np.append(
                np.nan*np.ones(6),tool_xform.array.reshape(16)))
        repl = struct.unpack('B', self._invoke_task(pkg))
        return repl[0]
    
    def check_arm_tool_path(self, q, tool_pose):
        pkg = task_qp_struct.pack(*np.append(q,tool_pose.array.reshape(16)))
        repl = struct.unpack('B6d', self._invoke_task(pkg))
        return repl[0],np.array(repl[1:])

    def check_tool_pose(self, tool_pose):
        pkg = task_p_struct.pack(tool_pose.array.reshape(16))
        repl = struct.unpack('B6d', self._invoke_task(pkg))
        return repl[0],np.array(repl[1:])

    def check_arm_pose(self, q):
        pkg = task_q_struct.pack(q)
        repl = struct.unpack('B', self._invoke_task(pkg))
        return repl[0]

if __name__ == '__main__':
    tc = TargetChecker()
    tc.start()
    import atexit
    atexit.register(tc.stop)
    # tcl = TargetCheckerClient()
    # q=tc._cm.robot_facade.cmd_joint_pos
    # tp= tc._cm.robot_facade.frame_computer()
    # tcl.check(q,tp)
    import __main__
    if hasattr(__main__, '__file__'):
        import code
        code.interact(banner='', local=globals())
