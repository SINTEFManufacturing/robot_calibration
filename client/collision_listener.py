"""
Module for listening to emulation based collision checker.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPL"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import struct
import socket

class CollEventListener(threading.Thread):
    def __init__(self, emu_addr=('127.0.0.1', 10099)):
        self._emu_addr = emu_addr
        threading.Thread.__init__(
            self, 
            name='{}<{}>'.format(self.__class__.__name__, self._emu_addr))
        self.daemon = True
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__stop = False
        self._coll_state = None
        self._coll_time = None
        self._coll_accum = False
        
    def clear_accum(self):
        self._coll_accum = self._coll_state
        
    def _exchange(self, cmd):
        self._socket.sendto(cmd.encode(), self._emu_addr)
        #self._coll_event_time, self._coll_state = struct.unpack('fB',self._socket.recv(1024))

    def run(self):
        self._exchange('connect')
        while not self.__stop:
            self._coll_time, self._coll_state = struct.unpack('fB',self._socket.recv(1024))
            self._coll_accum += self._coll_state

    @property
    def current_collision(self):
        return self._coll_state

    @property
    def accum_collision(self):
        return self._coll_accum > 0

    def stop(self):
        self.__stop = True
        self._exchange('disconnect')

""" Test code """
if __name__ == '__main__':
    cel = CollEventListener()
    cel.start()
    # cel.stop()
